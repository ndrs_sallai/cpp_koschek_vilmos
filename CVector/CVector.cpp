#include "CVector.h"

template CVector<int>;
template CVector<char>;
template CVector<double>;

template <class T>
CVector<T>::~CVector() {
	delete[] m_pData;
}

template <class T>
CVector<T>::CVector(int n) {
}

template <class T>
void CVector<T>::SetSize(int n) {
	// ez a tan�r �r implement�ci�ja:
	if (m_pData) {
		delete[] m_pData;
	}
	Init(n);

	// el�g fura, hiszen t�rli az eg�sz t�mb�t �s egy �j m�rettel l�trehoz egy �reset
}

template <class T>
T& CVector<T>::operator[](int n) {
	if (n >= m_iSize) {
		throw std::invalid_argument("index out of range");
	}

	return m_pData[n];
}