#pragma once

// "exception" class
class CVectorIndexOutOfRange {};

template <class T> class CVector
{
public:
	CVector(int n);
	~CVector();
	void SetSize(int n);
	T& operator[](int n);
protected:
	void Init(int n) {
		m_pData = new T[n];
		m_iSize = n;
	}
	T * m_pData;
	int m_iSize;
};