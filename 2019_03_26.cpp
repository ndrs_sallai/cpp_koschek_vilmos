/*
 * kivételkezeléssel kapcsolatban
 */
Test2::Test2()
{
	Test1 *pt = new Test1("auto_ptr");
	auto_ptr<Test1> pt2(pt); // felszabadítja a memoriát
	auto_ptr<Test1> pt3 = pt2; // pt2 NULL!
	cout << "\nmsg:" << pt3->msg; // hozzáférés az adattaghoz
	Test1 *pt4 = pt3.get();
	k(); // kivételt dob, itt pt3-nak meghívódik a destruktora
	
	// a trükk lényege, hogy csinálnak egy tamplatet, ami nem pointer, de úgy viselkedik
}


/*
 * mystring.h mystring.cpp main.cpp
 * Ezt kellene otthon megcsinálni (az implementációt), ez lesz még 1-2 óra témája.
 * Ahol lehet/érdemes, exception-nel hibakezelés!
 * Az exception osztályt lehet a mystring.h header fileban definiálni.
 */
class CMyString {
private:
	char * m_pchData;
	int    m_nDataLength;
public:
	CMyString();
	CMyString(const char * psz);
	CMyString(char ch, int nRepeat = 1);
	~CMyString();

	int GetLength() const;
	char GetAt(int nIndex) const;
	void SetAt(int nIndex, char ch);
	void Append(const char *psz);
	void Display() const;
	void Reverse();
};