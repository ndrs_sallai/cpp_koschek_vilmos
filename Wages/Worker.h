#pragma once
#include "Employee.h"

class Worker : public Employee {
public:
	void Init() {
		Employee::Init();

		std::cout << "hourly wage: " << std::endl;
		std::cin >> this->hourly_wage;

		std::cout << "hours: " << std::endl;
		std::cin >> this->hours;
	}

	int GetSalary() {
		return this->hourly_wage * this->hours;
	}
protected:
	int hourly_wage;
	int hours;
};
