#pragma once
#include "Employee.h"

class Manager : public Employee {
public:
	void Init() {
		Employee::Init();

		std::cout << "weekly wage: " << std::endl;
		std::cin >> this->weekly_wage;

		std::cout << "weeks: " << std::endl;
		std::cin >> this->weeks;
	}

	int GetSalary() {
		return this->weekly_wage * this->weeks;
	}
protected:
	int weekly_wage;
	int weeks;
};