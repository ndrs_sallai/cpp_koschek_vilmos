#include <iostream>
#include "Employee.h"
#include "Worker.h"
#include "Agent.h"
#include "Manager.h"

int main() {
	int n_employees;
	char menu_code = 'e'; // doesn't matter

	std::cout << "how many employees?" << std::endl;
	std::cin >> n_employees;

	Employee** employees = new Employee*[n_employees];

	while (menu_code != 'q') {
		// show menu

		std::cout << "Menu: " << std::endl;
		std::cout << "Add employees" << std::endl;
		std::cout << "List employees" << std::endl;
		std::cout << "Show wages" << std::endl;

		std::cin >> menu_code;

		if (menu_code == 'a') {
			for (size_t i = 0; i < n_employees; i++)
			{
				char type_code;
				Employee *the_new_one = new Employee;

				std::cout << "what type of worker? (w|m|a) " << std::endl;
				std::cin >> type_code;

				switch (type_code) {
				case 'w':
					the_new_one = new Worker;
					break;
				case 'm':
					the_new_one = new Manager;
					break;
				case 'a':
					the_new_one = new Agent;
					break;
				default:
					std::cout << "invalid type";
					continue;
				}

				the_new_one->Init();

				employees[i] = the_new_one;
			}
		}

		if (menu_code == 'q') {
			// free up memory
			for (size_t i = 0; i < n_employees; i++)
			{
				delete employees[i];
			}
			return 0;
		}

		if (menu_code == 'l') {
			for (size_t i = 0; i < n_employees; i++)
			{
				std::cout << "name: " << employees[i]->GetName() << std::endl;
			}
		}

		if (menu_code == 's') {
			char *name = nullptr;
			std::cout << "which employee? " << std::endl;
			std::cin >> name;

			for (size_t i = 0; i < n_employees; i++)
			{
				if (employees[i]->GetName() == name) {
					std::cout << "The emplyee's salary is: " << employees[i]->GetSalary();
				}
			}
		}
	}

	return 0;
}