#pragma once
#include "Worker.h"

class Agent : public Worker {
public:
	void Init() {
		Worker::Init();

		std::cout << "commission rate: " << std::endl;
		std::cin >> this->commission_rate;

		std::cout << "n sold: " << std::endl;
		std::cin >> this->n_sold;
	}

	int GetSalary() {
		return this->hourly_wage * this->hours + this->commission_rate * this->n_sold;
	}
protected:
	int hourly_wage;
	int hours;
	int commission_rate;
	int n_sold;
};