#include <iostream>
#include <conio.h>
#pragma warning(disable:4996)

struct big_struct {
	char a[1000];
	int n;
} bs = { "asdf", 5 };

int main() {
	val(bs);
	std::cout << "\n" << bs.n;
	ptr(&bs);
	ref(bs); // note: ugyan�gy h�vjuk meg, mint ha �rt�k szerint adn�m �t!
}

void val(big_struct v) {
	std::cout << "\n" << v.n++;
	std::cout << "\n" << v.a;
}

void ptr(big_struct *p) {
	std::cout << "\n" << p->n++;
	std::cout << "\n" << p->a;
}

void ref(big_struct &r) {
	std::cout << "\n" << r.n++;
	std::cout << "\n" << r.a;
}