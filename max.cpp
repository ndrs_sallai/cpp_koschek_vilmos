#include <iostream>
#define MAX(A,B) ((A) > (B) ? (A) : (B))

inline int max(int a, int b) {
	if (a > b) return a;
	return b;
}

/*void main() {
	int i, x = 23, y = 45;
	i = max(x++, y++);
	std::cout << "x=" << x;
	x = 23; y = 45;
	i = MAX(x++, y++);
	std::cout << "y=" << y;
}*/