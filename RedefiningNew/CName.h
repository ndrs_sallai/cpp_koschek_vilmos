#pragma once

class CName {
	int n;

public:
	void * operator new(size_t size);
	void operator delete(void*);
};