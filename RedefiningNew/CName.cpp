#include <iostream>

#include "CName.h"

const int POOL_SIZE = 5;

char memory_pool[POOL_SIZE][sizeof(CName)];
char pool_flags[POOL_SIZE]{ 0,0,0,0,0 };

void * CName::operator new(size_t size) {
	for (size_t i = 0; i < POOL_SIZE; i++) {
		if (pool_flags[i] == 0) {
			// mark as used
			pool_flags[i] = 1;
			
			// return pointer to beginning of unused space
			return &memory_pool[i * size];
		}
	}
}

void CName::operator delete(void *obj) {
	pool_flags[((char*)obj - memory_pool[0]) / sizeof(CName)] = 0;
}