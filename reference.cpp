#include <iostream>
#include <conio.h>
#pragma warning(disable:4996)

int main() {
	int i = 10;
	int & ref_i = i;

	std::cout << "\n" << &i << "\t" << &ref_i;

	ref_i++;

	std::cout << "\n" << i;

	getch();
	return(0);
}