#include <iostream>
#include <time.h>
#include <conio.h> // getch() fv van benne, ezzel tartjuk életben a programot futtatáskor
#pragma warning(disable:4996)

using namespace std;

void display_time(const struct tm *tim) {
	cout << "1 date: " << asctime(tim) << "\n";
}

void display_time(const time_t *tim) {
	cout << "2 date: " << ctime(tim) << "\n";
}

void main() {
	time_t tim = time(NULL);
	struct tm *ltim = localtime(&tim);
	display_time(&tim);
	display_time(ltim);
	getch();
}